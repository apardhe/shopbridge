# README #

### ShopBridge.Api Overview

Below are the technologies used in the application

- .NET Core 3.1 
- SQL Server local mdf file as database (database mdf file is located in App_Data/Db/ShopBridge.mdf location).

### Web Api documentation 

- WebApi enpoint documentation can be viewed by visiting below swagger url - 
- {domain url}/swagger/index.html - Eg. http://192.168.0.103:90/swagger/index.html

### Deployment guidlines

- In order to host the WebApi on IIS server, you must install [.NET Core Hosting Bundle](https://dotnet.microsoft.com/permalink/dotnetcore-current-windows-runtime-bundle-installer) on server. 
- In root directory of repository, you can find deployment package in Release folder.

### Application level log information

- Application level logs are maintained in App_Data/Logs folder
