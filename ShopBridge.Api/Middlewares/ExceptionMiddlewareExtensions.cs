﻿using Microsoft.AspNetCore.Builder;
using ShopBridge.Api.Middlewares.CustomExceptionMiddleware;

namespace GlobalErrorHandling.Extensions
{
    public static class ExceptionMiddlewareExtensions
    {
        public static void ConfigureCustomExceptionMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionMiddleware>();
        }
    }
}