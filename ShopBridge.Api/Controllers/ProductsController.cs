﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ShopBridge.Api.Models.Products;
using ShopBridge.Api.Services.Product;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShopBridge.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductService _productService;
        private readonly ILogger<ProductsController> _logger;

        public ProductsController(IProductService productService, ILogger<ProductsController> logger)
        {
            _productService = productService;
            _logger = logger;
        }

        [HttpGet]
        [Route("GetProducts")]
        public async Task<List<ProductDto>> GetProducts()
        {
            return await _productService.GetProducts();
        }

        [HttpPost]
        [Route("AddProduct")]
        public async Task AddProduct(AddProductDto input)
        {
            await _productService.AddProduct(input);
        }

        [HttpPost]
        [Route("UpdateProduct")]
        public async Task UpdateProduct(UpdateProductDto input)
        {
            await _productService.UpdateProduct(input);
        }

        [HttpPost]
        [Route("DeleteProduct/{id}")]
        public async Task DeleteProduct(int id)
        {
           await _productService.DeleteProduct(id);
        }
    }
}
