﻿using System.ComponentModel.DataAnnotations;

namespace ShopBridge.Api.Models.Products
{
    public class AddProductDto
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public int Stock { get; set; }
        public bool IsActive { get; set; }
    }
}
