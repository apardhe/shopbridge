﻿namespace ShopBridge.Api.Models.Products
{
    public class ProductDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Stock { get; set; }
        public bool IsActive { get; set; }
    }
}
