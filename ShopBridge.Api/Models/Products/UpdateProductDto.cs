﻿using System.ComponentModel.DataAnnotations;

namespace ShopBridge.Api.Models.Products
{
    public class UpdateProductDto
    {
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public int Stock { get; set; }
        public bool IsActive { get; set; }
    }
}
