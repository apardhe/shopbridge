﻿using Microsoft.EntityFrameworkCore;
using ShopBridge.Api.DatabaseContext;
using ShopBridge.Api.Models.Products;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DbProduct = ShopBridge.Api.DatabaseContext.Product;

namespace ShopBridge.Api.Services.Product
{
    public class ProductService : IProductService
    {
        private readonly SbDbContext _sbDbContext;

        public ProductService(SbDbContext sbDbContext)
        {
            _sbDbContext = sbDbContext;
        }

        public async Task<List<ProductDto>> GetProducts()
        {
            var products = await _sbDbContext.Products.Select(p => new ProductDto
            {
                Id = p.Id,
                Name = p.Name,
                Description = p.Description,
                Stock = p.Stock,
                IsActive = p.IsActive
            }).ToListAsync();

            return products;
        }

        public async Task AddProduct(AddProductDto input)
        {
            var newProduct = new DbProduct
            {
                Name = input.Name,
                Description = input.Description,
                IsActive = input.IsActive,
                Stock = input.Stock
            };

            _sbDbContext.Products.Add(newProduct);

            await _sbDbContext.SaveChangesAsync();
        }

        public async Task UpdateProduct(UpdateProductDto input)
        {
            var product = _sbDbContext.Products.Find(input.Id);

            product.Name = input.Name;
            product.Description = input.Description;
            product.Stock = input.Stock;
            product.IsActive = input.IsActive;

            await _sbDbContext.SaveChangesAsync();
        }

        public async Task DeleteProduct(int id)
        {
            var product = _sbDbContext.Products.Find(id);

            _sbDbContext.Products.Remove(product);

            await _sbDbContext.SaveChangesAsync();
        }
    }
}
