﻿using ShopBridge.Api.Models.Products;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShopBridge.Api.Services.Product
{
    public interface IProductService
    {
        Task<List<ProductDto>> GetProducts();

        Task AddProduct(AddProductDto input);

        Task DeleteProduct(int id);

        Task UpdateProduct(UpdateProductDto input);
    }
}
