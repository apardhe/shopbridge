﻿#nullable disable

namespace ShopBridge.Api.DatabaseContext
{
    public partial class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Stock { get; set; }
        public bool IsActive { get; set; }
    }
}
