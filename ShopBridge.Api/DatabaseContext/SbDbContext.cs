﻿using Microsoft.EntityFrameworkCore;

#nullable disable

namespace ShopBridge.Api.DatabaseContext
{
    public partial class SbDbContext : DbContext
    {
        public SbDbContext()
        {
        }

        public SbDbContext(DbContextOptions<SbDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Product>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(200);
            });
           
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
